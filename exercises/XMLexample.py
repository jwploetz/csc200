import xml.etree.ElementTree as ET
data = """
<techjobs>
    <person>
        <name>Ploetz</name>
        <job>ASM</job>
        <side>Stage Right</side>
        <cookie>Snickerdoodle</cookie>
    </person>
    <person>
        <name>Sam</name>
        <job>ASM</job>
        <side>Stage Left</side>
        <cookie>Chocolate chip</cookie>
    </person>
    <person>
        <name>Fred</name>
        <job>flies</job>
        <side>Stage Right</side>
        <cookie>Sugar cookie</cookie>
    </person>
    <person>
        <name>JFK</name>
        <job>doesnt do theater :,( </job>
        <side>N/A</side>
        <cookie>All the cookies</cookie>
    </person>
    <person>
        <name>Bob</name>
        <job>Run</job>
        <side>Stage Right</side>
        <cookie>Macaron</cookie>
    </person>
    <person>
        <name>Gary</name>
        <job>run</job>
        <side>Stage Left</side>
        <cookie>Muffins</cookie>
    </person>
</techjobs>
"""

tree = ET.fromstring(data)
lst = tree.findall('person')
print('Amount of techies: ', len(lst))
print('...............................................')
for item in lst:
    print('Name: ', item.find('name').text)
    print('Job: ', item.find('job').text)
    print('Stage Side: ', item.find('side').text)
    print('Favorite Cookie: ', item.find('cookie').text)
    print('...............................................')
