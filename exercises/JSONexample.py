import json
yummy = '''
[
    {
        "name" : "Chocolate Chip",
        "ingredients" : ["chocolte chips", "butter", "flour", "sugar", "eggs"],
        "isgood" : "Yes"
    },
    {
        "name" : "Snickerdoodle",
        "ingredients" : ["butter", "flour", "sugar", "eggs", "cinnamon", "cream of tartar"],
        "isgood" : "Yes"
    },
    {
        "name" : "Peanut butter cookies",
        "ingredients" : ["butter", "flour", "sugar", "eggs", "peanut butter"],
        "isgood" : "No"
    },
    {
        "name" : "Macaroon",
        "ingredients" : ["coconut", "egg whites", "sugar"],
        "isgood" : "Yes"
    },
    {
        "name" : "Macaron",
        "ingredients" : ["egg whites", "sugar", "almond flour", "filling"],
        "isgood" : "Yes"
    }
]'''

cookies = json.loads(yummy)
for item in cookies:
    print('')
    print(f'Cookie: {item["name"]}')
    print(f'Ingredients: {item["ingredients"]}')
    print(f'Is it tasty?: {item["isgood"]}')
    print('...............................................')
