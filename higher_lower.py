from random import randint

def guess_num():
    global guess
    try:
        guess = int(input("\nMake a guess: "))
    except:
        print("That wasn't a number")

def run():
    global guess
    print("OK, I've thought of a number between 1 and 1000.")
    number = randint(1,1000)
    guesses = 0
    guess_num()
    while guess != number:
        if guess > number:
            print("That's too high.")
        if guess < number:
            print("That's too low.")
        guesses += 1
        guess_num()
    print("That was my number. Well done!")
    print(f"\nYou took {guesses+1} guesses.")
    if input("Should we play again (yes or no)? ") == "yes":
        run()
    else:
        print("OK. Bye!")

run()
