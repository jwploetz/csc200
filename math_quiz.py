import random

score = 0
for i in range(10):
    num1, num2 = random.randint(0, 12), random.randint(0, 12)
    answer = num1 * num2

    if int(input(f"What's {num1} times {num2}? ")) == answer:
        print("That's right - well done.")
        score += 1
    else:
        print(f"No, I’m afraid the answer is {answer}.")

print(f"I asked you 10 questions. You got {score} of them right.")
print(f"Well done!")
