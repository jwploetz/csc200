1.  What is a wire protocol? What does serialization mean? Which two wire protocols does Dr. Chuck say we are going to look at?  
*Wire protocol is a way of getting data from point to point, serialization is the process of translating a data structure or object state into a format that can be stored or transmitted and reconstructed later. Dr. Chuck says we will look at XML and JSON*

2.  What is XML? When does Dr. Chuck say it first became popular?  
*XML stands for Extensible Markup Language, it first became popular in the 90s*

3.  Define each of these XML terms: element, tag, and attribute. Use web resources to help you come up with a more complete definition than the brief ones provided in the video.  
*element - XML data, tag - indicate the beginning and ending of elements, attribute - Keyword/value pairs on the opening tag of XML*

4.  Dr. Chuck gives us a synonym for element and talks about two types: simple and complex. Explain what he says about these.  
*The synonym is nodes, simple nodes are tags a some data, and complex nodes are simple nodes inside of other nodes.*
