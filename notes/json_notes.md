1.  Why does Dr. Chuck say you will encounter more JSON than XML as you go out there, and what does he say each of these notations are best used for?  
*XML is good for storing a large amount of data. JSON is better for when you want a simple way to save some data*  

2.  At the beginning of class today, I talked to you about the image Dr. Chuck shows us of JavaScript: The Definitive Guide next to Douglas Crockford's famous JavaScript: The Good Parts. Why is this funny and how does it relate to the history of JavaScript and its emergence as serious programming language?  
*The joke about JavaScript being overly complicated and having lots of extra features. However if you keep things simple JavaScript is easy to use.*  

3.  What does JSON stand for and where does it come from?  
*JavaScript Object Notation and was made by Douglas Crockford*  

4.  Which two Python data types correspond to the data representation of JSON?  
*Lists and dictionaries*  

5.  Douglas Crockford begins his interview by saying that JSON is .... Complete this sentence.  
*JSON is the worlds best loved data interchage format*  

6.  When did Douglas Crockford discover JSON?  
*2001, he says that the creation of JSON was like him naming and describing something that already existed in nature*  

7.  What is AJAX, and how does it relate to the history of JSON presented in this interview? Do a bit of web research on this one.  
*AJAX is a style of web development. Data is sent and received between a web browser and a web server in the background without changing the HTML webpage. JSON is used and was created to facilitate this communication*  

8.  How did Douglas Crockford make JSON into a standard?  
*He got JSON.org and stated it is a standard*  

9.  What does Douglas Crockford say was his boldest design decision in creating JSON? Why does he say this is its best feature?  
*in XML a request is sent to the server for data and then the client has to do further processing of this data. JSON is structed so that the second step of data processing is unnecessary*
