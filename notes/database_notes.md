1.  In the first video, Dr. Chuck gives us a brief history of the evolution of data processing. What examples did he use? Summarize his brief history with an even briefer one in your own word.  
*In the old days of computing there was not much storage. Storage was common in the forms of tape drives that would be sequential. Sorters where developed to sort data on these tapes to disk drives. This worked but was quite slow. The example he used was updating the balance of a bank account*  

2.  What does Dr. Chuck say is the advantage of relational databases that led eventually to their becoming popular?  
*Relational databases use powerful mathematics to be more efficient, which is why they became popular*  

3.  "SQL is not a procedural language it is an imperative language" says Dr. Chuck. Do a bit of web research and explain in a bit more detail what he means.  
*Imperative languages uses sequences of statements to determine how to reach an end goal.*  

4.  What is CRUD?  
*Create, read, update, delete*  

5.  In a large scale web application, the developer and the DBA are often separate roles.  Explain.  
*The developer works on the development of the application and the DBA works on the management of the database*  

6.  Who is E.F. Codd and why am I asking this question? (note: this is not covered in Dr. Chuck's lecture).  
*Edgar F. Codd was an English computer scientist who invented the relational model for databases while working at IBM. You are asking this question because he is the inventor of relational model for databases.*
