What is object-oriented programming (OOP)? When was it created, and why?  
**OOP is a programming concept centered around the concept of objects(data) instead of logic or functions. It focuses on what developers want to manipulate, rather than how they want to do it. Simula was the first OOP language, which was created in 1962 for doing simulations.**  

What are the principle characteristics of OOP?  
**Encapsulation - Private state of objects**  
**Abstraction - Using high level mechanisms to manipulate and use objects**  
**Inheritance - Reuse common logic while maintaining uniqueness of objects**
**Polymorphism - Use class exactly like parents, but each child keeps its own methods**  

How is OOP supported in Python?  
**Classes are used for OOP in Python. Classes are a way of easily defining and creating functions and methods for objects.**
