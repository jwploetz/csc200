
**Library:**  
A library is like a box full of precompiled routines that a program can use. They contain non-volatile resources such as "configuration data, documentation, help data, message templates, pre-written code and subroutines, classes, etc." They can be useful to shorten the length of your code and make it easier to read, and also make it possible to easily use the same code across different projects.  

**Research:**  
[en.wikipedia.org](https://en.wikipedia.org/wiki/Library_(computing))  
[www.webopedia.com](https://www.webopedia.com/definitions/library/)  
[www.encyclopedia.com](https://www.encyclopedia.com/computing/dictionaries-thesauruses-pictures-and-press-releases/program-library)

---  
**Framework:**  
Frameworks are platforms used to build applications. They are tools used programming easier when making larger projects. Frameworks often are designed to solve a specific purpose, and have built in classes and functions. This makes development faster because you don't need to code everything from scratch each time you code something ne.

**Research:**  
[en.wikipedia.org](https://en.wikipedia.org/wiki/Software_framework)  
[makemeaprogrammer.com](https://makemeaprogrammer.com/what-is-a-programming-framework/)  
[techterms.com](https://techterms.com/definition/framework)
